import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {  Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { IDemoRequest } from "../model/IDemoRequest";
import { IRequestDemo } from "../model/IRequestDemo";


const DEMO_REQUEST = '/payment/demo/request';
const SEND_DEMO = '/profile/demo/request';

@Injectable({
    providedIn: 'root'
  })
  export class PaymentService {
  
    constructor(private http: HttpClient) { }

postDemoRequest(data): Observable<IRequestDemo>{
    return this.http.post<IRequestDemo>(environment.apiUrlPayment + DEMO_REQUEST, data)
    .pipe(
        catchError(this.handleError)
        );
}

postSendDemoRequest(demo) : Observable<IDemoRequest>{

  return this.http.post<IDemoRequest>(environment.apiUrlProfile + SEND_DEMO, demo);
}

private handleError(error: HttpErrorResponse){
return throwError(() => error.error.message);
}
}