import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  IBusinessOwnerRegister,
  ICountEmployeesByCompany,
  IEmployeeRegisterList,
  IEmployeeRegisterListView,
} from '../model';
import { environment } from '../../../environments/environment';
import { ICountCoursesByCompany } from '../model/ICountCoursesByCompany';
import { ICountCoachesByCompany } from '../model/ICountCoachesByCompany';
import { ICourseNameEnroll } from '../model/ICourseNameEnroll';
import { ICoachSessions } from 'ICoachSessions';
import { IBOStat } from '../model/IBOStat';

const REGISTER = '/auth/register';
const COMPANY_NAME = '/company/name';
const EMPLOYEE_CREATE = '/profile/employee/create';
const EMPLOYEES_BY_COMPANY = '/profile/get/employees-by-company';
const COUNT_EMPLOYEES_BY_COMPANY = '/profile/count/employees-by-company';
const COUNT_COURSES_BY_COMPANY = '/courses/count';
const COUNT_COACHES_BY_COMPANY = '/coaches/coaches/count';
const BUSINESS_OWNER_STATISTIC = '/payment/bo-statistic';

@Injectable({
  providedIn: 'root',
})
export class BusinessOwnerService {
  constructor(private http: HttpClient) {}

  initRegister(
    user: IBusinessOwnerRegister
  ): Observable<IBusinessOwnerRegister> {
    return this.http.post<IBusinessOwnerRegister>(
      environment.apiUrlAuth + REGISTER,
      user
    );
  }

  finishRegister(companyName: string): Observable<any> {
    return this.http.post<any>(environment.apiUrlProfile + COMPANY_NAME, {
      companyName,
    });
  }

  employeeRegister(
    employees: IEmployeeRegisterList
  ): Observable<IEmployeeRegisterList> {
    return this.http.post<IEmployeeRegisterList>(
      environment.apiUrlProfile + EMPLOYEE_CREATE,
      employees
    );
  }

  getAllEmployeeByCompany({page,limit}): Observable<IEmployeeRegisterListView> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('limit', limit);
    return this.http.get<IEmployeeRegisterListView>(
      environment.apiUrlProfile + EMPLOYEES_BY_COMPANY,
      { params: queryParams }
    );
  }

  getCountEmployeesByCompany():Observable<ICountEmployeesByCompany>{
      return this.http.get<ICountEmployeesByCompany>(environment.apiUrlProfile+COUNT_EMPLOYEES_BY_COMPANY);  
  }

  getCountCoursesByCompany():Observable<ICountCoursesByCompany>{
    return this.http.get<ICountCoursesByCompany>(environment.apiUrlCourses + COUNT_COURSES_BY_COMPANY);  
}

getCountCoachesByCompany():Observable<ICountCoachesByCompany>{
  return this.http.get<ICountCoachesByCompany>(environment.apiUrl + COUNT_COACHES_BY_COMPANY);  
}

getBusinessOwnerStat(): Observable<IBOStat>{

  return this.http.get<IBOStat>(environment.apiUrl + BUSINESS_OWNER_STATISTIC);
}
}
