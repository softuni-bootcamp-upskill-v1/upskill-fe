import {Injectable} from '@angular/core';
import {Workbook} from 'exceljs';
import * as fs from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExportExcelService {
  constructor() {
  }

  exportExcel(excelData): void {
    const title = excelData.title;
    const header = excelData.headers;
    const data = excelData.data;
    const worksheetName = excelData.wsName;
    const total = excelData.total;

    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet(worksheetName);

    worksheet.mergeCells('A1', 'C3');
    const titleRow = worksheet.getCell('A1');
    titleRow.value = title;
    titleRow.font = {
      name: 'Calibri',
      size: 16,
      underline: 'single',
      bold: true,
      color: {argb: '4167B8'}
    };
    titleRow.alignment = {vertical: 'middle', horizontal: 'center'};

    const headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, num) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {argb: '4167B8'},
        bgColor: {argb: ''}
      };
      cell.font = {
        bold: true,
        color: {argb: 'FFFFFF'},
        size: 18
      };
      cell.style.border = {top: {style: 'thick'}, left: {style: 'thin'}, bottom: {style: 'thin'}, right: {style: 'thin'}};
    });

    data.forEach(d => {
      const row = worksheet.addRow(d);
      row.height = 40;
      const color = 'white';
      row.eachCell((cell, num) => {
        // cell.fill = {
        //   type: 'pattern',
        //   pattern: 'solid',
        //   fgColor: {argb: color},
        //   bgColor: {argb: ''}
        // },
        cell.font = {
          bold: false,
          color: {argb: 'black'},
          size: 18
        };
        cell.style.border = {
          top: {style: 'thick'}, left: {style: 'thin'},
          bottom: {style: 'thin'}, right: {style: 'thin'}
        };
      });
    });
    const totalArr = ['Total:', '', total];
    const totalRow = worksheet.addRow(totalArr);
    totalRow.height = 40;
    totalRow.eachCell((cell, num) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {argb: '4167B8'},
        bgColor: {argb: ''}
      };
      cell.font = {
        bold: true,
        color: {argb: 'black'},
        size: 18
      };
      // FFFFFF
      cell.style.border = {
        top: {style: 'thick'}, left: {style: 'thin'},
        bottom: {style: 'thin'}, right: {style: 'thin'}
      };
    });
    for (let rowIndex = 1; rowIndex <= worksheet.rowCount; rowIndex++) {
      worksheet.getRow(rowIndex).alignment = {vertical: 'middle', horizontal: 'center'};
    }
    worksheet.columns.forEach((column, i) => {
        let maxLength = 0;
        column.eachCell({includeEmpty: true}, (cell) => {
          const columnLength = cell.value ? cell.value.toString().length : 10;
          if (columnLength > maxLength) {
            maxLength = columnLength;
          }
        });
        column.width = maxLength < 10 ? 10 : maxLength;
      });

    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
      fs.saveAs(blob, title + '.xlsx');
    });
  }
}
