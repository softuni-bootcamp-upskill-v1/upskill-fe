import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {AdminDashboardRevenue} from '../model/admin-dashboard-revenue';
import {Observable} from 'rxjs';
import {IClientsAdminDashboard} from '../model/admin-clients-dashboard';
import * as http from 'http';
import {IRevenueModel} from '../model/revenue-model-view';
import {ICompanyModel} from '../model/admin-company-clients';
import {IBoInvoiceModel} from '../model/bo-invoices-model';


const ADMIN_COUNT_COURSES = '/courses/count-courses';
const ADMIN_COUNT_COACHES = '/coaches/coaches/count-coaches';
const ADMIN_COUNT_CLIENTS = '/profile/count-clients';
const MONTHLY_REPORTS = '/payment/monthly-report';
const CLIENTS_ADMIN_DASHBOARD = '/company/clients-by-month';
const CLIENTS_ADMIN = '/company/get-clients';
const REVENUES_PAGEABLE = '/payment/revenues';
const BO_INVOICES = '/payment/bo-invoices';

@Injectable({
  providedIn: 'root',
})
export class AdminService {

  constructor(private http: HttpClient) {
  }

  getCountOfCourses() {
    return this.http.get(environment.apiUrlCourses + ADMIN_COUNT_COURSES);
  }

  getCountOfCoaches() {
    return this.http.get(environment.apiUrl + ADMIN_COUNT_COACHES);
  }

  getCountOfClients() {
    return this.http.get(environment.apiUrlProfile + ADMIN_COUNT_CLIENTS);
  }

  getMonthlyReports(): Observable<AdminDashboardRevenue> {
    return this.http.get<AdminDashboardRevenue>(environment.apiUrl + MONTHLY_REPORTS);
  }

  getClientsForAdminDashboard(): Observable<IClientsAdminDashboard> {
    return this.http.get<IClientsAdminDashboard>(environment.apiUrlProfile + CLIENTS_ADMIN_DASHBOARD);
  }

  getRevenues(page, size): Observable<Array<IRevenueModel>> {

    let params = new HttpParams();
    params = params.appendAll({page, size});

    return this.http.get<Array<IRevenueModel>>(environment.apiUrl + REVENUES_PAGEABLE, {params});
  }

  getClients(page, size): Observable<Array<ICompanyModel>> {

    let params = new HttpParams();
    params = params.appendAll({page, size});

    return this.http.get<Array<ICompanyModel>>(environment.apiUrlProfile + CLIENTS_ADMIN, {params});
  }

  getBO_Invoices(month, year): Observable<Array<IBoInvoiceModel>> {
    let params = new HttpParams();
    params = params.appendAll({month, year});
    return this.http.get<Array<IBoInvoiceModel>>(environment.apiUrl + BO_INVOICES, {params});
  }

}
