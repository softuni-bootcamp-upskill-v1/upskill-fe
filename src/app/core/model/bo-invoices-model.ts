export interface IBoInvoiceModel {
  modelName: string,
  date: string,
  price: number,
}
