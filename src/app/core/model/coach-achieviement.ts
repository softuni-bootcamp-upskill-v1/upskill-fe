export interface ICoachAchievement {
  name: string;
  startDate: Date;
  endDate: Date;
  session: number;
}
