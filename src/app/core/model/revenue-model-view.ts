export interface IRevenueModel {
  name: string;
  money: number;
}
