export interface IRequestDemo {
  fullName: string;
  email: string;
  companyName: string;
  phoneNumber: string;
}
