export interface ICourseNameEnroll{
    name: string,
    enrolled: number
}