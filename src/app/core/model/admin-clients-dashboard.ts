export interface IClientsAdminDashboard {
  counts: number [],
  months: string [],
  monthByNumber: number [],
}
