import { ICoachSessions } from "ICoachSessions";
import { ICourseNameEnroll } from "./ICourseNameEnroll";

export interface IBOStat{
    courses: Array<ICourseNameEnroll>;
    coaches: Array<ICoachSessions>;
}