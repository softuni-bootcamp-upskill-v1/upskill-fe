export interface ISession {
  id: string;
  start: Date;
  available: boolean;
}
