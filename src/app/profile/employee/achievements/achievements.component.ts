import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ICoachAchievement } from 'src/app/core/model/coach-achieviement';
import { CourseService } from 'src/app/core/service';
import ICourseAchievement from 'src/app/course/interfaces/ICourseAchieviement';
import {CoachService} from '../../../core/service/coach.service';


@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.scss']
})
export class AchievementsComponent implements OnInit {

  coursesAchievement$: Observable<Array<ICourseAchievement>>;
  dataSource: Array<ICourseAchievement> = [];
  displayedColumns  = ['name', 'startDate', 'endDate', 'grade'];

  coachAchievement$: Observable<Array<ICoachAchievement>>;
  dataSourceCoach: Array<ICoachAchievement> = [];
  displayedCoachColumns  = ['name', 'startDate', 'endDate', 'session'];

   constructor(private courseService: CourseService, private coachService: CoachService, ) { }

  ngOnInit(): void {
    this.coursesAchievement$ = this.courseService.getCoursesAchievements();
    this.coursesAchievement$.subscribe((data) => {this.dataSource = data; console.log(this.dataSource); });
    this.coachAchievement$ = this.coachService.getCoachAchievements();
    this.coachAchievement$.subscribe((data) => {this.dataSourceCoach = data; });
  }


}
