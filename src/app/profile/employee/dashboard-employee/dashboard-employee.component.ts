import { Component, OnInit } from '@angular/core';
import { catchError, map, mergeMap, Observable, of, take, tap } from 'rxjs';
import { IEmployeeCourse } from 'src/app/core/model/employee-course';
import { CourseService } from 'src/app/core/service';
import { CoachService } from 'src/app/core/service/coach.service';
import { ICoachesCreate } from '../../../core/model/coaches-create';

@Component({
  selector: 'app-dashboard-employee',
  templateUrl: './dashboard-employee.component.html',
  styleUrls: ['./dashboard-employee.component.scss'],
})
export class DashboardEmployeeComponent implements OnInit {
  randomCourses: IEmployeeCourse[];
  randomCoaches: ICoachesCreate[];

  constructor(
    private coachService: CoachService,
    private courseService: CourseService
  ) {}

  ngOnInit(): void {
    this.coachService
      .getCompanyOwnerEmail()
      .pipe(
        take(1),
        mergeMap((e) => {          
          return this.coachService.getEmployeeRandomCoaches(e);
        }),
        mergeMap((c) => {
          this.randomCoaches = c as ICoachesCreate[];
          console.log('second merge', this.randomCoaches);
          return this.courseService.getEmployeeRandomCourses();
        }),
        map((r) => {
          this.randomCourses = r as IEmployeeCourse[];
          console.log('map', this.randomCourses);
          return this.randomCourses;
        }),
        catchError((error) => {
          console.log(error);
          return of(error);
        })
      )
      .subscribe();
  }
}
