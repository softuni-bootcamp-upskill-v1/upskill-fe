import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { logout } from '../../../+store/login/action';
import { Router } from '@angular/router';
import { BusinessOwnerService, CourseService } from 'src/app/core/service';
import { ICountEmployeesByCompany } from 'src/app/core/model';
import { Observable } from 'rxjs';
import { ICountCoursesByCompany } from 'src/app/core/model/ICountCoursesByCompany';
import { ICountCoachesByCompany } from 'src/app/core/model/ICountCoachesByCompany';
import { ICourseNameEnroll } from 'src/app/core/model/ICourseNameEnroll';
import { ICoachSessions } from 'ICoachSessions';


@Component({
  selector: 'app-business-owner-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

countEmployees: ICountEmployeesByCompany;
countCourses: ICountCoursesByCompany;
countCoaches: ICountCoachesByCompany;
coursesEnrolled: Array<ICourseNameEnroll>;
coachesSessions: Array<ICoachSessions>;
pageCoaches: Number = 1;
pageCourses: Number = 1;
totalRecordsCoaches: number;
totalRecordsCourses: number;

  constructor(private store: Store, private router: Router, private boService: BusinessOwnerService, private courseService: CourseService ) {}

  ngOnInit(): void {

   this.boService.getCountEmployeesByCompany().subscribe((data)=> this.countEmployees = data);
    this.boService.getCountCoursesByCompany().subscribe((data) => this.countCourses = data);
    this.boService.getCountCoachesByCompany().subscribe((data) => this.countCoaches = data);

    this.boService.getBusinessOwnerStat().subscribe((data) => {

      this.coachesSessions = data.coaches;
      this.totalRecordsCoaches = data.coaches.length;
      this.coursesEnrolled = data.courses;
      this.totalRecordsCourses = data.courses.length;

    });
  }

  logout() {
    this.store.dispatch(logout());
    localStorage.removeItem('token');
    this.router.navigate(['home']);
  }
}
