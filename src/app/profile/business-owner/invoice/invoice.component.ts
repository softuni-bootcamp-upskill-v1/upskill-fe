import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AdminService} from '../../../core/service/admin.service';
import {IBoInvoiceModel} from '../../../core/model/bo-invoices-model';
import {jsPDF} from 'jspdf';
import {ExportExcelService} from '../../../core/service/excel.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  date = new Date();
  year = this.date.getFullYear();
  month = this.date.getMonth() + 1;
  monthForPdf = this.getMonth(this.month);
  yearForPdf = this.year;
  lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
  monthTitle = this.getMonth(this.month);
  invoices: Array<IBoInvoiceModel>;
  total = 0;
  excelData;

  @ViewChild('htmlData', {static: false}) htmlData!: ElementRef;

  constructor(private adminService: AdminService, private excelService: ExportExcelService) {
  }

  ngOnInit(): void {

    this.adminService.getBO_Invoices(this.month, this.year).subscribe(data => {
      this.invoices = data;
      let money = 0;
      data.forEach(s => money += s.price);
      this.total = money;
    });
  }

  getPreviousMonth() {
    this.getMonthAndYearDataPrevious();
    this.invoices = [];
    this.adminService.getBO_Invoices(this.month, this.year).subscribe(data => {
      this.invoices = data;
      let money = 0;
      data.forEach(s => money += s.price);
      this.monthTitle = this.getMonth(this.month);
      this.total = money;
    });
  }

  getNextMonth() {
    this.getMonthAndYearDataNext();
    this.invoices = [];
    this.adminService.getBO_Invoices(this.month, this.year).subscribe(data => {
      this.invoices = data;
      let money = 0;
      data.forEach(s => money += s.price);
      this.monthTitle = this.getMonth(this.month);
      this.total = money;
      console.log(this.invoices);
    });
  }

  getMonthAndYearDataPrevious() {
    if (this.month - 1 < 1) {
      this.month = 12;
      this.year -= 1;
    } else {
      this.month--;
    }

  }

  getMonthAndYearDataNext() {
    if (this.month + 1 > 12) {
      this.month = 1;
      this.year += 1;
    } else {
      this.month++;
    }
  }

  makePDF(): void {
    const pdf = new jsPDF('p', 'pt', 'a4');
    pdf.html(this.htmlData.nativeElement, {
      callback: (pdf) => {
        pdf.save(`invoice-${this.monthForPdf}-${this.yearForPdf}.pdf`);
      }
    });
  }
  exportExcel(): void {
    const data = [];
    this.invoices.forEach((row: any) => {
      data.push(Object.values(row));
    });
    const reportData = {
      title: `Invoice-${this.getMonth(this.month)}-${this.year}`,
      data,
      headers: ['Coach/Course', 'Date', 'Price'],
      wsName: `invoice-${this.getMonth(this.month)}-${this.year}`,
      total: this.total
    };
    this.excelService.exportExcel(reportData);
  }

  getMonth(month): string {
    switch (month) {
      case 1:
        return 'January';
      case 2:
        return 'February';
      case 3:
        return 'Mart';
      case 4:
        return 'April';
      case 5:
        return 'May';
      case 6:
        return 'June';
      case 7:
        return 'July';
      case 8:
        return 'August';
      case 9:
        return 'September';
      case 10:
        return 'October';
      case 11:
        return 'November';
      case 12:
        return 'December';
    }
  }
}
