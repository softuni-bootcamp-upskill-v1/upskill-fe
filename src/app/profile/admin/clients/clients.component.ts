import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../core/service/admin.service';
import {IRevenueModel} from '../../../core/model/revenue-model-view';
import {ICompanyModel} from '../../../core/model/admin-company-clients';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  page = 1;
  size = 3;
  info: Array<ICompanyModel>;
  hide = false;

  constructor(private adminService: AdminService) {
  }

  ngOnInit(): void {
    this.adminService.getClients(this.page, this.size).subscribe(data => {
      this.info = data;
    });
  }

  getMoreClients() {
    this.page++;
    const length = this.info.length;
    this.adminService.getClients(this.page, this.size).subscribe(data => {
      const clients: Array<ICompanyModel> = data;
      this.info.push(...clients);
      if (length === this.info.length) {
        this.hide = true;
      }
    });
  }
}
