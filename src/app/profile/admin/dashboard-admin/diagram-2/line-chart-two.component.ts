import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../../core/service/admin.service';

@Component({
  selector: 'app-line-chart-two',
  templateUrl: './line-chart-two.component.html',
  styleUrls: ['./line-chart-two.component.scss']
})
export class LineChartTwoComponent implements OnInit {
  revenues;
  months;

  constructor(private adminService: AdminService) {
  }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public lineChartLabels;
  public lineChartType = 'line';
  public lineChartLegend = false;
  public lineChartData;

  ngOnInit(): void {
    this.adminService.getMonthlyReports().subscribe(d => {
      this.revenues = d.revenues;
      this.months = d.months;
      this.lineChartData = [
        {data: [...this.revenues], label: 'Revenue', borderColor: 'green' , backgroundColor: 'green'}
      ];
      this.lineChartLabels = [...this.months];
    });
  }
}
