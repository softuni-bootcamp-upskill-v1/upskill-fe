import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../../core/service/admin.service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  clientsCount;
  clientsMonths;


  constructor(private adminService: AdminService) {
  }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public lineChartLabels;
  public lineChartType = 'line';
  public lineChartLegend = false;
  public lineChartData;
  ngOnInit() {
    this.adminService.getClientsForAdminDashboard().subscribe(info => {
      this.clientsCount = info.counts;
      this.clientsMonths = info.months;
      this.lineChartData = [
        {data: [...this.clientsCount], label: 'Clients', borderColor: 'blue', backgroundColor: 'blue'}
      ];
      this.lineChartLabels = [...this.clientsMonths];
    });
  }

}
