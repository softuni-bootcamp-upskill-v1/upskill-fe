import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../core/service/admin.service';
import {AdminDashboardRevenue} from '../../../core/model/admin-dashboard-revenue';


@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.scss']
})
export class DashboardAdminComponent implements OnInit {

  countOfCourses;
  countOfCoaches;
  countOfClients;
  revenue$;

  constructor(private adminService: AdminService) {
  }

  ngOnInit(): void {
    this.adminService.getCountOfCourses().subscribe(d => this.countOfCourses = d);
    this.adminService.getCountOfCoaches().subscribe(d => this.countOfCoaches = d);
    this.adminService.getCountOfClients().subscribe(d => this.countOfClients = d);
    this.adminService.getMonthlyReports().subscribe(d => {
      const r = d.revenues ? d.revenues[d.revenues?.length - 1] : 0;
      this.revenue$ = r;
    });
  }
}
