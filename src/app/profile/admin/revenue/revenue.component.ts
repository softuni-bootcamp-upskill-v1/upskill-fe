import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../core/service/admin.service';
import {IRevenueModel} from '../../../core/model/revenue-model-view';

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.scss']
})
export class RevenueComponent implements OnInit {

  page = 1;
  size = 2;
  info: Array<IRevenueModel>;
  total = 0;
  hide = false;

  constructor(private adminService: AdminService) {
  }


  ngOnInit(): void {
    this.adminService.getRevenues(this.page, this.size).subscribe(data => {
      this.info = data;
      let money = 0;
      data.forEach(s => money += (s.money * 0.5));
      this.total = money;
    });
  }

  getMoreRevenues() {
    this.page++;
    const length = this.info.length;
    this.adminService.getRevenues(this.page, this.size).subscribe(data => {
      const revenues: Array<IRevenueModel> = data;
      this.info.push(...revenues);

      let money = 0;
      data.forEach(s => money += (s.money * 0.5));
      this.total += money;
      if (length === this.info.length) {
        this.hide = true;
      }
    });
  }
}
