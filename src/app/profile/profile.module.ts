import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeRegisterComponent } from './business-owner/employee-register/employee-register.component';
import { NgxCsvParserModule } from 'ngx-csv-parser';
import { DashboardComponent } from './business-owner/dashboard/dashboard.component';
import { EmployeesComponent } from './business-owner/employees/employees.component';
import { DashboardEmployeeComponent } from './employee/dashboard-employee/dashboard-employee.component';
import { DashboardAdminComponent } from './admin/dashboard-admin/dashboard-admin.component';
import { ClientsComponent } from './admin/clients/clients.component';
import { RevenueComponent } from './admin/revenue/revenue.component';
import { InvoiceComponent } from './business-owner/invoice/invoice.component';
import { AchievementsComponent } from './employee/achievements/achievements.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgrxDispatcherModule } from 'ngrx-dispatcher';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { NgChartsModule } from 'ng2-charts';
import { LineChartComponent } from './admin/dashboard-admin/diagram/line-chart-component';
import { LineChartTwoComponent } from './admin/dashboard-admin/diagram-2/line-chart-two.component';
import { ProfileCommonModule } from '../profile-common/profile-common.module';
import { CourseModule } from '../course/course.module';
import { CoachModule } from '../coach/coach.module';
import { NgxPaginationModule } from 'ngx-pagination';
import {EmployeeCatalogComponent} from './employee/employee-catalog/employee-catalog.component'

const materialModules = [
  MatDialogModule,
  MatSidenavModule,
  MatCardModule,
  MatToolbarModule,
  MatListModule,
  MatDividerModule,
  MatInputModule,
  MatButtonModule,
  MatTableModule,
];

@NgModule({
  declarations: [
    LineChartTwoComponent,
    LineChartComponent,
    EmployeeRegisterComponent,
    DashboardComponent,
    EmployeesComponent,
    DashboardAdminComponent,
    DashboardEmployeeComponent,
    ClientsComponent,
    RevenueComponent,
    InvoiceComponent,
    AchievementsComponent,
    EditProfileComponent,
    EmployeeCatalogComponent
  ],
  imports: [
    NgChartsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCsvParserModule,
    NgrxDispatcherModule,
    NgxCsvParserModule,
    NgxPaginationModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    ProfileCommonModule,
    CoachModule,
    CourseModule,
    ...materialModules,
    RouterModule.forChild([
      { path: 'employee-register', component: EmployeeRegisterComponent },
      { path: 'dashboard-company', component: DashboardComponent },
      { path: 'dashboard-employee', component: DashboardEmployeeComponent },
      { path: 'dashboard-admin', component: DashboardAdminComponent },
      { path: 'clients', component: ClientsComponent },
      { path: 'revenues', component: RevenueComponent },
      { path: 'employees', component: EmployeesComponent },
      { path: 'invoice', component: InvoiceComponent },
      { path: 'achievements', component: AchievementsComponent },
      { path: 'employee/catalog', component: EmployeeCatalogComponent },
    ]),
  ],
  exports: [
    EmployeeRegisterComponent,
    DashboardComponent,
    EmployeesComponent,
    DashboardAdminComponent,
    DashboardEmployeeComponent,
  ],
  entryComponents: [EditProfileComponent],
})
export class ProfileModule {}
