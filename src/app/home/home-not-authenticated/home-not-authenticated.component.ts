import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IDemoRequest } from 'src/app/core/model/IDemoRequest';
import { PaymentService } from 'src/app/core/service/payment.service';
import { YoutubeService } from 'src/app/core/service/youtube.service';

@Component({
  selector: 'app-home-not-authenticated',
  templateUrl: './home-not-authenticated.component.html',
  styleUrls: ['./home-not-authenticated.component.scss']
})
export class HomeNotAuthenticatedComponent implements OnInit {

  demoForm: FormGroup;
  demo: IDemoRequest;
  channels:any;
  channelId!: string;
  playlistLength!: number;
  videoId!: string;
  videoUrl!: string;

  constructor( private fb: FormBuilder, private paymentService: PaymentService, private toastr: ToastrService, private youtubeService: YoutubeService) { }

  ngOnInit(): void {
    this.demoForm = this.fb.group({
      fullName: ['', [Validators.minLength(4)]],
      companyName: ['', [Validators.minLength(2)]],
      email: ['', [Validators.email]],
      phoneNumber: ['', [Validators.min(10)]]
    });
  }

  onRequestDemo(){

    this.paymentService.postDemoRequest(this.demoForm.value).subscribe((data) => {
     
      this.sendEmail(this.demoForm.value.email);
        this.toastr.success('Demo has been requested successfully!');
        this.demoForm.reset();
    }, (error) => {
        this.toastr.error(error, 'Invalid data');
    });
  }

  sendEmail(email: string){

    this.youtubeService.getChannels("Programming").subscribe((data)=> {
      this.channels = data.items;
      let index = this.generateRandomNumber(4);
      this.channelId = data.items[index].id.channelId;

      this.youtubeService.getChannelVideos(this.channelId).subscribe((data) => {
        this.playlistLength = data.pageInfo.resultsPerPage;
        let index = this.generateRandomNumber(this.playlistLength - 1);
        this.videoId = data.items[index].id.videoId;
        this.videoUrl = "http://www.youtube.com/watch?v=" + this.videoId;

        let demoRequest: IDemoRequest = {
          email: email,
          videoUrl: this.videoUrl
      }
      this.paymentService.postSendDemoRequest(demoRequest).subscribe((data) => {console.log(data)});
       });
    });
  }

  private generateRandomNumber(upperBound: number){
    return Math.floor((Math.random() * upperBound) + 1);
  }

  get c(): any {
    return this.demoForm.controls;
  }
}
