import {ChangeDetectorRef, Input} from '@angular/core';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { CalendarEvent, CalendarEventTitleFormatter, CalendarView } from 'angular-calendar';
import {
  subDays,
  startOfMonth,
  endOfMonth,
  startOfWeek,
  endOfWeek,
  startOfDay,
  endOfDay,
  format
} from 'date-fns';
import {ISession} from '../../core/model/session-view';
import {ATTENTION, ERROR_MESSAGE} from '../constants/constants';
import {CoachService} from '../../core/service/coach.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {CalenderComponent} from '../calender/calender.component';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-calendar-header',
  templateUrl: './calendar-header.component.html',
  styleUrls: ['./calendar-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CalendarEventTitleFormatter,
    },
  ],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarHeaderComponent implements OnInit {

  viewDate = new Date();
  activeDayIsOpen = false;
  @Input('events')
  events: CalendarEvent[];
  constructor(private cdr: ChangeDetectorRef,
              private coachService: CoachService,
              private router: Router,
              private toastr: ToastrService,
              private dataService: CalenderComponent) {}
  view: CalendarView = CalendarView.Month;
  @Input() locale = 'en';
  @Input('coach')
  coach: string;

  ngOnInit(): void {
    this.initDays();
    console.log('from header' + this.events);
  }

  initDays(): void {

    const getStart: any = {
      month: startOfMonth,
      week: startOfWeek,
      day: startOfDay
    }[this.view];

    const getEnd: any = {
      month: endOfMonth,
      week: endOfWeek,
      day: endOfDay
    }[this.view];

  }

  bookSession(id): void {
    this.coachService.bookCoachSession(this.coach, id).subscribe((data) => {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/employee-profile']);
    }, err => {
      this.toastr.error(ERROR_MESSAGE, ATTENTION, {timeOut: 5000000});
    });
  }
}
