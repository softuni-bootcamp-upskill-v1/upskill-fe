import {Component, EventEmitter, OnInit, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CoachService} from '../../core/service/coach.service';
import {AuthService} from '../../core/service';
import {Observable} from 'rxjs';

import {Router} from '@angular/router';

@Component({
  selector: 'app-coach-details',
  templateUrl: './coach-details.component.html',
  styleUrls: ['./coach-details.component.scss']
})
export class CoachDetailsComponent implements OnInit {

  isEmployee$: Observable<boolean> = this.authService.isEmployee$();
  @Output() onChange = new EventEmitter<any>();

  constructor(private coachService: CoachService, private authService: AuthService, private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: { details: any, hasBackdrop: true, disableClose: true }) {
  }

  ngOnInit(): void {
  }

  bookSession(id: string, $event): any {
    this.coachService.bookCoach(id).subscribe((data) => {
      this.onChange.emit($event);
    });
  }

  reloadComponent(): void {
    const currentUrl = this.router.url;
    console.log(this.router.url);
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }


}
