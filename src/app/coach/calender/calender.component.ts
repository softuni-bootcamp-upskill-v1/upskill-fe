import {Component, EventEmitter, Injectable, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {ISession} from '../../core/model/session-view';
import {CoachService} from '../../core/service/coach.service';
import {ActivatedRoute, Router} from '@angular/router';
import {error} from 'protractor';
import {ToastrService} from 'ngx-toastr';
import {ATTENTION, ERROR_MESSAGE} from '../constants/constants';
import {CalendarEvent} from 'angular-calendar';
import {startOfDay} from 'date-fns';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss']
})

export class CalenderComponent implements OnInit {

  sessions$: Observable<Array<ISession>>;
  events: CalendarEvent[] = [];
  idCoach: string;
  @Output() onChange = new EventEmitter<any>();
  error: null;
  now: Date = new Date();

  constructor(private coachService: CoachService, private route: ActivatedRoute, private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(data => {
      this.idCoach = data.id;
      this.coachService.getAllCoachSession(this.idCoach).subscribe((results) => {
        results.forEach((res) => {
          const meetDate = new Date(res.start);
          this.now.setHours(12, 0, 0, 0);
          meetDate.setHours(12, 0, 0, 0);
          if (res.available) {
              if (meetDate > this.now) {
                const event = {
                  start: startOfDay(meetDate),
                  title: 'Meeting',
                  id: res.id
                };
                this.events.push(event);
              }
          }
        });
      });
    });
  }


  reloadChange($event): void {
    this.sessions$ = this.coachService.getAllCoachSession(this.idCoach); }
}
