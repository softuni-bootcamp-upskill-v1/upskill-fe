import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProfileAsideComponent } from './profile-aside/profile-aside.component';

@NgModule({
  declarations: [ProfileAsideComponent],
  imports: [CommonModule, RouterModule],
  exports: [ProfileAsideComponent],
})
export class ProfileCommonModule {}
