import {IEmployeeRegisterState} from '../interface/state'
export const getAllEmployeeByCompany = (state: IEmployeeRegisterState) => state.employees;
export const getEmployeesCountByCompany = (state: IEmployeeRegisterState) => state.countEmployees;
export const getEmployeeCountFromStore = (state: IEmployeeRegisterState) =>  state.employees.length;

