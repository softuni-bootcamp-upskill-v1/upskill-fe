
interface ICourseAchieviement {
    name: string;
    startDate: Date;
    endDate: Date;
    grade: string;
}

export default ICourseAchieviement;