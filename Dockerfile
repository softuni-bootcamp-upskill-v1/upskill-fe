FROM node:14-alpine as build-angular
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm build
RUN npm run build
EXPOSE 4200
CMD ["npm", "start"]


